package box;

import java.io.File;

import java.util.ArrayList;
import java.util.Arrays;

public class Box{
    private final double length, height, width;
    private final Item[] items;
    private final double cost;
    private final int    numItems;

    private final double maxLength, maxHeight, maxWidth;

    private Box(ArrayList<Item> items, double maxLength, double maxWidth,
            double maxHeight){
        if(items==null) throw new NullPointerException("The given item list "+
            "is invalid");
        if(maxWidth>maxLength || maxHeight>maxWidth) throw new
            IllegalArgumentException(String.format("Invalid Limits: Lenght sh"+
                        "ould be bigger than height and width, and width sho"+
                        "uld be bigger than height. (%.2f %.2f %.2f)",maxLength,
                        maxWidth,maxHeight));

        this.maxLength = maxLength;
        this.maxWidth  = maxWidth;
        this.maxHeight = maxHeight;

        ArrayList<Item> group = new ArrayList<Item>(items.size());
        for(Item item : items){
            if(item.getLength() < maxLength && item.getWidth() < maxWidth &&
                    item.getHeight() < maxHeight){
                group.add(item);
            }
        }
        this.items = Arrays.copyOf(group.toArray(new Item[1]),group.size());
        numItems = this.items.length;

        for(Item item : this.items) items.remove(item);

        double[] boxDimensions = generateBox();
        height = boxDimensions[0];
        width  = boxDimensions[1];
        length = boxDimensions[2];

        double size = 1.0 * numItems * length + 2 * width + 2 * height;

        if(size<250){
            cost = size * 0.01; 
        }else if(size < 500){
            cost = size * 0.02; 
        }else if(size < 1000){
            cost = size * 0.05; 
        }else if(size < 1500){
            cost = size * 0.10; 
        }else if(size < 2000){
            cost = size * 0.25; 
        }else{
            cost = size * 0.50; 
        }
    }

    // TODO Calculate box dimensions based on cushion curve information
    private double[] generateBox(){
        double[] dim = new double[3];
        for(int i=0;i<3;i++) dim[i] = Double.MIN_VALUE;

        for(Item item : items){
            if(item.getHeight() > dim[0]) dim[0] = item.getHeight();
            if(item.getWidth()  > dim[1]) dim[1] = item.getWidth();
            if(item.getLength() > dim[2]) dim[2] = item.getLength();
        }

        return dim;
    }

    public double getLength(){ return length; }

    public double getWidth() { return width;  }

    public double getHeight(){ return height; }

    public double getCost(){ return cost; }

    public int getItemNumber(){ return numItems; }

    public Item[] getItems(){
        return Arrays.copyOf(items,items.length);
    }

    @Override
    public String toString(){
        String s = "";
        s += String.format("Dimensions: L = %.1f W = %.1f H = %.1f [mm]\n",
                length,width,height);
        s += String.format("Unit Cost: %.2f\n",cost/numItems);
        s += String.format("Nº Objects: %d\nTotal Cost: %.2e\n",numItems,cost);
        return s;
    }

    public static Box[] createBoxes(Item[] items, double[][] limits){
        if(items==null) throw new NullPointerException("Items list cannot be "+
            "null.");
        if(items.length == 0) throw new IllegalArgumentException("Items list "+
            "can't be empty.");

        if(limits==null) throw new NullPointerException("Limits cannot be "+
            "null.");
        if(limits.length == 0) throw new IllegalArgumentException("Invalid "+
            "limits: Empty array.");
        for(double[] limit : limits) if(limit.length<1 || limit.length>3)
            throw new IllegalArgumentException("Invalid limits: There sould be"+
                    " at least 1 dimension for each limit and 3 dimensions at "+
                    "most.");

        ArrayList<Item> available = new ArrayList<Item>(items.length);
        for(Item item : items) available.add(item);

        Box[] boxes = new Box[limits.length+1];
        for(int i=0;i<limits.length;i++){
            double[] limit = limits[i];
            if(limit.length == 3)
                boxes[i] = new Box(available,limit[0],limit[1],limit[2]);
            else if(limit.length == 2)
                boxes[i] = new Box(available,limit[0],limit[1],limit[1]);
            else
                boxes[i] = new Box(available,limit[0],limit[0],limit[0]);
        }
        double max = Double.MAX_VALUE;
        boxes[limits.length] = new Box(available,max,max,max);

        return boxes;
    }

    // Test Method
    public static void main(String[] args){
        File fp = new File(args[0]);
        Item[] items = Item.readFromFile(fp).toArray(new Item[1]);

        //System.out.printf("%d items read from file.\n\n",items.length);

        double[][] lim1 = new double[1][3];
        lim1[0][0] = lim1[0][1] = lim1[0][2] = 520.74;
        Box[] boxes1 = createBoxes(items,lim1);

        System.out.println("\nBoxes 1:");
        double cost = 0;
        for(Box box : boxes1){
            System.out.printf("Box: %.1f x %.1f x %.1f\n",box.length,box.height,
                    box.width);
            cost += box.getCost();
        }
        System.out.printf("Total cost: %.2e\n",cost);

        double[][] lim2 = new double[2][3];
        lim2[0][0] = lim2[0][1] = lim2[0][2] = 260.87;
        lim2[1][0] = lim2[1][1] = lim2[1][2] = 800.60;
        Box[] boxes2 = createBoxes(items,lim2);

        System.out.println("\nBoxes 2:");
        cost = 0;
        for(Box box : boxes2){
            System.out.printf("Box: %.1f x %.1f x %.1f\n",box.length,box.height,
                    box.width);
            cost += box.getCost();
        }
        System.out.printf("Total cost: %.2e\n",cost);

        /*
        double min = 1.0;
        double max = 2000;
        double[][] lim = new double[2][3];
        for(int i=0;i<100;i++){
            for(int j=i;j<100;j++){
                lim[0][0] = lim[0][1] = lim[0][2] =
                        min + i * .01 * (max - min);
                lim[1][0] = lim[1][1] = lim[1][2] =
                        min + j * .01 * (max - min);
                Box[] boxes = createBoxes(items,lim);

                double cost = 0.0;
                for(Box box : boxes) cost += box.getCost();

                System.out.printf("%e,%e,%e\n",lim[0][0],lim[1][0],cost);
            }
        }
        */
    }
}
