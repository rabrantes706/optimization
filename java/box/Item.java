package box;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Scanner;

import java.net.URL;
import java.net.MalformedURLException;

public class Item{
    private final String name;
    private final URL    url;

    private final double length, height, width;
    private final double weight;
    private final double fragility;

    private final boolean slimObject;
    private final boolean randomWeight;

    public Item(String name, double[] dimensions, double weight, URL url){
        if(name == null) throw new NullPointerException("Item name must be a "+
                "valid name!");
        if(dimensions.length < 2) throw new IllegalArgumentException("Not "+
               "enough dimensions to create an valid Item object.");

        this.name = name.replaceAll(",","");
        this.url  = url;

        Arrays.sort(dimensions);
        if(dimensions.length == 2){
            slimObject = true;

            width = dimensions[0];
            length = dimensions[1];
            height = .01 * width;
        }else{
            slimObject = false;

            height = dimensions[0];
            width  = dimensions[1];
            length = dimensions[2];
        }

        if(width == 0 || height == 0 || length == 0)
            throw new IllegalArgumentException("Invalid dimensions: None of "+
                    "the given dimensions can be zero.");

        if(weight <= 0){
            randomWeight = true;
            this.weight = (0.5 * Math.random() + .95) * length * height * width
                * 1e-3;
        }else{
            randomWeight = false;
            this.weight = weight;
        }

        fragility = 60.0 * Math.random() + 35.0;
    }

    private Item(String name, double[] dim, double weight, double fragility,
            boolean isSlim, boolean isRandomWeight, URL url){

        this.name = name;
        this.url  = url;

        slimObject = isSlim;
        randomWeight = isRandomWeight;

        height = dim[0];
        width  = dim[1];
        length = dim[2];

        this.weight = weight;
        this.fragility = fragility;
    }

    public double getLength(){ return length; }

    public double getWidth() { return width;  }

    public double getHeight(){ return height; }

    @Override
    public String toString(){
        String out = name + ":\n";
        out += String.format("%.2f x %.2f x %.2f [mm]\n",length,width,height);
        out += String.format("%.2f [g]\n",weight);
        out += String.format("%.0f [G]\n",fragility);
        return out;
    }

    @Override
    public boolean equals(Object o){
        if(o == null) return false;
        if(this == o) return true;
        if(o instanceof Item){
            Item that = (Item)o;
            return that.name.equals(this.name) && that.length == this.length &&
                that.width == this.width && that.height == this.height;
        }
        return false;
    }

    @Override
    public int hashCode(){
        int hash = name.hashCode();
        hash *= 31+ Objects.hash(length,width,height);
        return hash;
    }

    public static boolean writeToFile(LinkedList<Item> list, File file){
        String data = "";
        for(Item item : list){
            data += String.format("%b,%b,%.2f,%.2f,%.2f,%.2f,%.2f,%s,%s\n",
                    item.slimObject, item.randomWeight, item.length, 
                    item.width, item.height, item.weight,item.fragility,
                    item.name,item.url.toString());
        }

        try(FileWriter fw = new FileWriter(file)){
            fw.write(data,0,data.length());
        }catch(IOException e){
            System.out.println("An error has occurred while writing Items to "
                    +"a file!\n");
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static LinkedList<Item> readFromFile(File file){
        LinkedList<Item> list = new LinkedList<Item>();
        try(Scanner sc = new Scanner(file)){
            sc.useDelimiter("[,\\v]+");

            while(sc.hasNextLine()){
                boolean slim = sc.nextBoolean();
                boolean randW = sc.nextBoolean();

                double[] dim = new double[3];
                dim[2] = sc.nextDouble();
                dim[1] = sc.nextDouble();
                dim[0] = sc.nextDouble();

                double weight = sc.nextDouble();
                double frag   = sc.nextDouble();

                String name = sc.next();
                String sURL = sc.next();
                sc.nextLine();

                URL url;
                try{
                    url = new URL(sURL);
                }catch(MalformedURLException e){
                    url = null;
                }

                Item item = new Item(name,dim,weight,frag,slim,randW,url);
                list.add(item);
            }
        }catch(FileNotFoundException e){
            System.out.printf("Error: The file %s does not exist.",
                    file.toString());
        }
        return list;
    }

    // Test method
    public static void main(String[] args){
        File fp = new File(args[0]);
        Item[] items = Item.readFromFile(fp).toArray(new Item[1]);

        Comparator<Item> byLength = (Item i1, Item i2) -> {
            if(i1.length>i2.length) return 1;
            else if(i1.length==i2.length) return 0;
            else return -1;
        };
        Arrays.sort(items,byLength);

        int count = 0;
        for(Item item : items) if(item.length > 1500.0) count++;
        System.out.printf("Number of Items: %d\n",items.length);
        System.out.printf("Number of Giant Items: %d\n",count);
/*
        LinkedList<Item> newItems = new LinkedList<Item>();
        for(Item item : items) {
            if(item.length <= 2000.0) newItems.add(item);
        }

        Item.writeToFile(newItems,fp);
*/
        for(int i=1;i<Integer.parseInt(args[1]);i++)
            System.out.println(items[items.length-i]);
    }
}
