package crawler;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.regex.Matcher;

import javax.swing.text.MutableAttributeSet; 
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

import box.Item;

class Crawler extends Thread{
    private final CrawlerManager cm;
    private final Object lock;

    private URL current;
    private LinkedList<URL> found;

    private int iVisited, iFound, iSuccessfull;

    {
        this.setDaemon(true);
        lock = new Object();
        iVisited = iFound = iSuccessfull = 0;
    }

    Crawler(CrawlerManager cm){
        this.cm = cm;
    }

    @Override
    public void run(){
        while(!Thread.interrupted()){
            while((current = cm.getURL()) != null){ 
                boolean again = true;
                int iAgain = 0;
                while(again && iAgain<10){
                    try(Reader reader = new InputStreamReader(
                                (InputStream)current.getContent());){

                        found = new LinkedList<URL>();
                        new ParserDelegator().parse(reader, new Scrapper(),
                                true);

                        again = false;
                        iVisited++;

                        cm.addURL(found.toArray(new URL[1]));
                        iFound += found.size();
                    }catch(IOException e){
                        again = true;
                        if(iAgain==10) System.out.printf("Error while getting " + 
                                "HTML from %s\n",current.toString());
                    }
                }
            }
            try{
                synchronized(lock){
                    wait();
                }
            }catch(InterruptedException e){}
        }
    }

    private class Scrapper extends HTMLEditorKit.ParserCallback{
        private boolean itemTitle, itemAttr, attrLabels, span, link;
        private String attributeLabel, attribute;
        private int divLevel;

        private String itemName;
        private double[] dim;
        private double weight;

        {
            itemAttr = itemTitle = attrLabels = span = link = false;
            attributeLabel = attribute = null;
            divLevel = -1;

            itemName = null;
            dim = null;
            weight = -1;
        }

        @Override
        public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos){
            if(t == HTML.Tag.H1){
                String val = (String)a.getAttribute(HTML.Attribute.ID);
                itemTitle = val != null && (val.equals("itemTitle"));
            }else if(t == HTML.Tag.DIV){
                if(!itemAttr){
                    String val = (String)a.getAttribute(HTML.Attribute.CLASS);
                    itemAttr = val != null && (val.equals("itemAttr"));
                    divLevel = 0;
                }else{
                    divLevel++;
                }
            }else if(itemAttr && t == HTML.Tag.TD){
                if(!attrLabels){
                    String val = (String)a.getAttribute(HTML.Attribute.CLASS);
                    attrLabels = val != null && (val.equals("attrLabels"));
                }
            }else if(t == HTML.Tag.SPAN){
                span = true;
            }else if(t == HTML.Tag.LI){
                if(!link){
                    String val = (String)a.getAttribute(HTML.Attribute.CLASS);
                    link = val != null && (val.contains("mfe-reco-ct"));
                }
            }else if(link && t == HTML.Tag.A){
                String site = (String)a.getAttribute(HTML.Attribute.HREF);
                try{
                    URL url = new URL(site);
                    found.add(url);
                }catch(MalformedURLException e){
                    //it is not important
                }
            }
        }

        @Override
        public void handleEndTag(HTML.Tag t, int pos){
            if(itemAttr && t == HTML.Tag.DIV){
                divLevel--;
                if(divLevel < 0){
                    divLevel = -1;
                    itemAttr = false;
                }
            }else if(itemTitle && t == HTML.Tag.H1){
                itemTitle = false;
            }else if(t == HTML.Tag.SPAN){
                span = false;
                if(attrLabels){
                    attrLabels = false;
                    attributeLabel = null;
                    attribute = null;
                }
            }else if(t == HTML.Tag.LI){
                link = false;
            }
        }

        @Override
        public void handleText(char[] data, int pos){
            if(itemTitle && !span){
                itemName = new String(data);
                itemTitle = false;
            }else if(itemAttr && attrLabels && !span){
                attributeLabel = new String(data);
            }else if(itemAttr && attrLabels && span){
                attribute = new String(data);
                parseAttribute(attributeLabel,attribute);
            }
        }

        @Override
        public void handleEndOfLineString(String eol){
            if(dim != null && dim.length > 1){
                try{
                    Item item = new Item(itemName,dim,weight,current);
                    cm.addItem(item);
                    iSuccessfull++;
                }catch(IllegalArgumentException e){
                    System.out.printf("\nCould not create item:\n%s\n",
                            e.getMessage());
                }
            }
        }

        private void parseAttribute(String label, String attribute){
            try{
                Pattern unitPattern = Pattern.compile("[^a-z]([cdm]?m|"+
                        "k?g[mr]?|in|lb|\"|'')[^a-z]");
                Matcher matcher = unitPattern.matcher(attribute.toLowerCase());
                String unit;
                if(matcher.find()){
                    unit = attribute.substring(matcher.start(), matcher.end());
                }else return;

                String labelLC = label.toLowerCase();
                if(labelLC.contains("size") || labelLC.contains("dimensions")){
                    String[] sDim = attribute.split("[^\\d\\.,]+");
                    if(sDim.length == 2 || sDim.length == 3){
                        dim = new double[sDim.length];
                        for(int i=0;i<sDim.length;i++)
                            dim[i] = toMillis(Double.parseDouble(sDim[i]),unit);
                    }
                }else if(labelLC.contains("length") || labelLC.contains("width")
                       || labelLC.contains("height")
                       || labelLC.contains("depth")){

                    String sDim = attribute.replaceAll("[^\\d\\.,]+","");
                    if(dim == null){
                        dim = new double[1];
                        dim[0] = toMillis(Double.parseDouble(sDim),unit);
                    }else{
                        double[] tempDim = Arrays.copyOf(dim,dim.length+1);
                        dim = tempDim;
                        dim[dim.length-1] = 
                            toMillis(Double.parseDouble(sDim),unit);
                    }
                }else if(labelLC.contains("weight")){
                    String sDim = attribute.replaceAll("[^\\d\\.,]+","");
                    weight = toGrams(Double.parseDouble(sDim),unit);
                }
            }catch(PatternSyntaxException e){
                System.out.println("Invalid unit regex pattern.");
                return;
            }catch(NumberFormatException e){
                System.out.printf("\nIncapable of parsing %s as valid " +
                       "dimensions: Invalid Number\n",attribute);
            }catch(IllegalArgumentException e){
                System.out.printf("\nIncapable of parsing %s as valid " +
                       "dimensions: \n",attribute);
                System.out.println(e.getMessage());
            }
       }

        private double toMillis(double val, String unit){
            if(unit.toLowerCase().contains("\"") ||
                    unit.toLowerCase().contains("''") ||
                    unit.toLowerCase().contains("in"))
                return val * 25.4;
            else if(unit.toLowerCase().contains("mm")) return val;
            else if(unit.toLowerCase().contains("cm")) return val * 10;
            else if(unit.toLowerCase().contains("dm")) return val * 100;
            else if(unit.toLowerCase().contains("m"))  return val * 1000;
            else throw new IllegalArgumentException("Invalid Unit: " + unit);
        }

        private double toGrams(double val, String unit){
            if(unit.toLowerCase().contains("lb")) return val * 453.59;
            else if(unit.toLowerCase().contains("kg")) return val * 1000;
            else if(unit.toLowerCase().contains("g") || 
                    unit.toLowerCase().contains("gm") ||
                    unit.toLowerCase().contains("gr"))
                return val;
            else throw new IllegalArgumentException("Invalid Unit: " + unit);
        }
    }

    int getVisited(){ return iVisited; }

    int getFound(){ return iFound; }

    int getSuccessfull(){ return iSuccessfull; }

    Object getLock(){ return lock; }

    // Test Method
    public static void main(String[] args){
        URL site = null;
        try{
            site = new URL(args[0]);
        }catch(java.net.MalformedURLException e){
            System.out.println("Invaild URL.");
            return;
        }
    }
}
