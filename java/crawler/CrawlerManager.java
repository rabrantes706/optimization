package crawler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import java.net.URL;
import java.net.MalformedURLException;

import java.util.LinkedList;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

import box.Item;

public class CrawlerManager{
    private final File fVisited;
    private final File fFound;
    private final File fItem;

    private final LinkedList<URL> lVisited;
    private final ArrayList<URL> lFound;
    private final LinkedList<Item> lItem;

    private final Object lock, urlLock, itemLock;

    private final Crawler[] workers;
    private final CrawlMonitor monitor;

    private int maxFoundLength;

    public CrawlerManager(String visited, String found, String item,
            int threadNumber){
        lock     = new Object();
        urlLock  = new Object();
        itemLock = new Object();

        fVisited = new File(visited);
        fFound   = new File(found);
        fItem    = new File(item);

        lVisited = new LinkedList<URL>();
        int iLine = 0;
        try(Scanner sc = new Scanner(fVisited)){
            while(sc.hasNextLine()){
                iLine++;
                String sLine = sc.nextLine();
                URL uLine = new URL(sLine);
                lVisited.add(uLine);
            }
        }catch(FileNotFoundException e){
            throw new IllegalArgumentException(
                String.format("The file %s does not exist!",visited));
        }catch(MalformedURLException e){
            System.out.printf("Malformed URL detected in file %s in line %d\n",
                visited,iLine);
        }

        lFound = new ArrayList<URL>();
        iLine = 0;
        try(Scanner sc = new Scanner(fFound)){
            while(sc.hasNextLine()){
                iLine++;
                String sLine = sc.nextLine();
                URL uLine = new URL(sLine);
                lFound.add(uLine);
            }
        }catch(FileNotFoundException e){
            throw new IllegalArgumentException(
                String.format("The file %s does not exist!",found));
        }catch(MalformedURLException e){
            System.out.printf("Malformed URL detected in file %s in line %d\n",
                found,iLine);
        }

        lItem = Item.readFromFile(fItem);

        int processors;
        if(threadNumber < 1){
            Runtime rt = Runtime.getRuntime();
            processors = rt.availableProcessors();
        }else{
            processors = threadNumber;
        }
        workers = new Crawler[processors];

        monitor = new CrawlMonitor();

        maxFoundLength = 0;
    }

    private class CrawlMonitor extends Thread{
        private int limit;
        private boolean print;
        private final Object lock;

        {
            lock = new Object();
            this.setDaemon(true);
            limit = 0;
        }

        protected void setLimit(int stopAt){
            limit = (stopAt<0)?Integer.MAX_VALUE:stopAt;
        }

        protected void setPrint(boolean value){
            print = value;
        }

        @Override
        public void run(){
            while(!Thread.interrupted()){
                try{
                    int alive, visited, found, success;
                    alive = visited = found = success = 0;

                    for(Crawler worker : workers){
                        if(worker.isAlive()) alive++;
                        else if(!Thread.interrupted())worker.getLock().notify();
                        visited += worker.getVisited();
                        found += worker.getFound();
                        success += worker.getSuccessfull();
                    }

                    if(print) System.out.printf("\r%2d out of %2d threads "+
                            "alive : %d Visited %d/%d Found %d/%d Successfully"+
                            " Read",alive, workers.length,visited,lFound.size(),
                            found, lItem.size(), success);
                    synchronized(lock){
                        lock.wait(500);
                    }
                }catch(InterruptedException e){
                    synchronized(lock){
                        lock.notify();
                    }
                }
            }
            System.out.println("\n\nCrawling has terminated\nHave a nice day");
        }
    }

    public void startCrawl(boolean status, int limit){
        for(int i=0;i<workers.length;i++){
            workers[i] = new Crawler(this);
            workers[i].start();
        }

        monitor.setLimit(limit);
        monitor.setPrint(status);
        monitor.start();
    }

    void setMaxFoundLength(int maxFound){
        maxFoundLength = (maxFound<1)? 0:maxFound;
        lFound.ensureCapacity(maxFound+100);
    }

    public void stopCrawl(){
        if(workers==null) throw new IllegalStateException("Cannot stop "+
                "crawling if it has not yet started.");

        for(Crawler worker : workers) worker.interrupt();

        monitor.interrupt();

        writeFiles();
    }

    private void writeFiles(){
        if(!Item.writeToFile(lItem,fItem)){
            System.out.println("Could not save items to file!");
        }

        try(FileWriter fwFound   = new FileWriter(fFound);
            FileWriter fwVisited = new FileWriter(fVisited)) {

            synchronized(urlLock){
                String data = "";
                for(URL url : lFound){
                    if(url != null) data += url.toString() + "\n";
                }
                fwFound.write(data,0,data.length());

                data = "";
                for(URL url : lVisited){
                    if(url != null) data += url.toString() + "\n";
                }
                fwVisited.write(data,0,data.length());
            }
        }catch(IOException e){
            System.out.println("An error occurred while writing to a file!\n");
            e.printStackTrace();
        }
    }

    URL getURL(){
        URL found;
        synchronized(urlLock){
	    try{
		do{
		    found = lFound.remove(0);
		}while(lFound.contains(found) || lVisited.contains(found) ||
                        found == null);
		lVisited.add(found);
	    }catch(NoSuchElementException e){
		found = null;
	    }
        }
        return found;
    }

    void addURL(URL[] url){
        if(maxFoundLength > 0 && lFound.size() >= maxFoundLength) return;
        synchronized(urlLock){
            for(URL u : url){
                if(!lVisited.contains(u) && !lFound.contains(u)) addURL(u);
            }
        }
    }
    private void addURL(URL url){
        synchronized(urlLock){
            lFound.add(url);
        }
    }

    void addItem(Item item){
        synchronized(itemLock){
            if(!lItem.contains(item)) lItem.add(item);
        }
    }

    // Test method
    public static void main(String[] args){
        CrawlerManager cm = new CrawlerManager(args[0],args[1],args[2],0);

        cm.setMaxFoundLength(1000);
        cm.startCrawl(true,0);

        Scanner sc = new Scanner(System.in);
        sc.nextLine();

        cm.stopCrawl();
    }
}
