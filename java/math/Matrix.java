/**
 * This class represents a matrix object for double precision values, with
 * basic matrix operations.
 *
 * @author RAbrantes
 */

package math;

import java.util.Arrays;

public class Matrix{
    private final double[][] values;
    private final int rows, columns;

    /**
     * Creates a new matrix with the values passed by a bidimensional array.
     *
     * @param array Bidimensional array with double precision values to
     * construct the new matrix with. If a ragged array is passed unknown
     * values will take the value of zero.
     */
    public Matrix(double[][] array){
        if(array==null) throw new NullPointerException("Invalid matrix constr"+
                "uctor array: Array cannot be null.");

        rows = array.length;
        int maxColumns = 0;
        for(int i=0;i<rows;i++){
            int length = array[i].length;
            maxColumns = (maxColumns>length)? maxColumns : length;
        }
        columns = maxColumns;

        if(rows == 0 || columns == 0) throw new IllegalArgumentException("Inv"+
                "alid matrix constructor array: Invalid Dimensions.");

        values = new double[rows][columns];
        for(int i=0;i<array.length;i++){
            values[i] = Arrays.copyOf(array[i],columns);
        }
    }

    /**
     * Create a new matrix of zeros with the given dimensions.
     *
     * @param rows Number of rows.
     * @param columns Number of columns.
     * @return New matrix populated with zeros.
     */
    public static Matrix zeros(int rows, int columns){
        double[][] array = new double[rows][columns];
        return new Matrix(array);
    }

    /**
     * Create a new square matrix of zeros with the given dimensions.
     *
     * @param size Size of the square matrix.
     * @return New square matrix populated with zeros.
     */
    public static Matrix zeros(int size){
        return zeros(size,size);
    }

    /**
     * Create a new matrix of zeros with the dimensions of the given matrix.
     *
     * @param matrix Matrix with the wanted dimensions.
     * @return New matrix of zeros with the given dimensions.
     */
    public static Matrix zeros(Matrix matrix){
        return zeros(matrix.rows(),matrix.columns());
    }
    /**
     * Create a new matrix filled with ones with the given dimensions.
     *
     * @param rows Number of rows for the new matrix.
     * @param columns Number of columns for the new matrix.
     * @return New square matrix of ones with the given dimensions.
     */
    public static Matrix ones(int rows, int columns){
        double[][] array = new double[rows][columns];
        for(int i=0;i<rows;i++) for(int j=0;j<columns;j++) array[i][j] = 1.0;
        return new Matrix(array);
    }

    /**
     * Create a new square matrix filled with ones with the given dimension.
     *
     * @param size The wanted dimension.
     * @return New square matrix of ones with the given dimension.
     */
    public static Matrix ones(int size){
        return ones(size,size);
    }

    /**
     * Create a new matrix filled with ones with the dimensions of the given
     * matrix.
     *
     * @param matrix Matrix with the wanted dimensions.
     * @return New matrix of ones with the given dimensions.
     */
    public static Matrix ones(Matrix matrix){
        return ones(matrix.rows(),matrix.columns());
    }

    /**
     * Create the identity matrix with the given size.
     *
     * @param size Integer value of the size of the matrix.
     * @return New square matrix with the diagonal of ones.
     */
    public static Matrix identity(int size){
        double[][] array = new double[size][size];

        for(int i=0;i<size;i++) for(int j=0;j<size;j++){
            if(j==i) array[i][j] = 1;
            else array[i][j] = 0;
        }

        return new Matrix(array);
    }

    /**
     * Get the canonical unit vectors that represent the space of the given 
     * rows*columns matrices.
     *
     * @param rows Number of rows of the matrices.
     * @param columns Number of columns of the matrices.
     * @return Array of matrices that represent a unit vector in each direction
     * on the space of the matrices with rows*columns dimensions.
     */
    public static Matrix[] unitVectors(int rows, int columns){
        Matrix[] unit = new Matrix[rows*columns];

        int k=0;
        for(int i=0;i<rows;i++) for(int j=0;j<columns;j++){
            double[][] array = new double[rows][columns];
            array[i][j] = 1;
            unit[k++] = new Matrix(array);
        }

        return unit;
    }

    /**
     * Get the unit vectors that represent the space of the given matrix.
     *
     * @param space Matrix with the dimensions to be taken to create the unit 
     * vectors.
     * @return Array of matrices that represent a unit vector in each direction
     * on the space of the given matrix.
     */
    public static Matrix[] unitVectors(Matrix space){
        return unitVectors(space.rows,space.columns);
    }

    /**
     * Calculate the result matrix of the sum of this matrix with the one passed
     * as argument.
     *
     * @param matrix The other matrix to sum.
     * @return New matrix with the sum result.
     */
    public Matrix sum(Matrix matrix){
        if(matrix.rows != rows || matrix.columns != columns)
            throw new IllegalArgumentException("Matrix dimensions don't agree");

        double[][] array = new double[rows][columns];
        for(int i=0;i<rows;i++){
            for(int j=0;j<columns;j++){
                array[i][j] = values[i][j] + matrix.values[i][j];
            }
        }

        return new Matrix(array);
    }

    /**
     * Calculate the cross product between this matrix and the one passed by 
     * parameter.
     *
     * @param matrix Right matrix.
     * @return A new matrix with the result of the cross product.
     */
    public Matrix product(Matrix matrix){
        if(this.columns != matrix.rows) throw new IllegalArgumentException(
                String.format("Invalid matrix dimensions: %dx%d and %dx%d",
                    this.rows,this.columns,matrix.rows,matrix.columns));

        double[][] array = new double[this.rows][matrix.columns];

        for(int i=0;i<rows;i++){
            for(int j=0;j<matrix.columns;j++){
                array[i][j] = this.getRow(i).dotProduct(matrix.getColumn(j));
            }
        }

        return new Matrix(array);
    }

    /**
     * Calculate the dot product of this matrix, representing a vector, and the
     * one passed by argument. Both matrices must represent vectors for this
     * method to work.
     *
     * @param matrix A matrix representing a vector.
     * @return Double value of the result of the dot product.
     */
    public double dotProduct(Matrix matrix){
        if(!(rows==1 || columns==1)) throw new UnsupportedOperationException(
                "This matrix cannot perform a dot product, it does not "+
                "represent a vector.");

        if(!(matrix.rows==1 || matrix.columns==1))
           throw new IllegalArgumentException("Given matrix must represent a "+
                   "vector.");

        if(rows*columns != matrix.rows*matrix.columns)
            throw new IllegalArgumentException("Dimensions don't agree.");

        double sum = 0;
        for(int i=0;i<rows*columns;i++){
            sum += getValue(i) * matrix.getValue(i);
        }

        return sum;
    }

    /**
     * Element by element product.
     *
     * @return 
     */
    public Matrix matrixDotProduct(Matrix matrix){
        if(rows != matrix.rows || columns != matrix.columns)
            throw new IllegalArgumentException(String.format("Matrix dimensio"+
                        "ns must agree (%dx%d %dx%d",rows,columns,matrix.rows,
                        matrix.columns));

        double[][] array = new double[rows][columns];
        for(int i=0;i<rows;i++) for(int j=0;j<columns;j++){
            array[i][j] = values[i][j] * matrix.values[i][j];
        }

        return new Matrix(array);
    }

    /**
     * Scalar matrix multiplication.
     *
     * @param a The scalar value to multiply this matrix.
     * @return A new matrix scaled by the given value.
     */
    public Matrix scale(double a){
        double[][] array = new double[rows][columns];

        for(int i=0;i<rows;i++){
            for(int j=0;j<columns;j++){
                array[i][j] = a * values[i][j];
            }
        }

        return new Matrix(array);
    }

    /**
     * Scalar matrix multiplication. If a value is less than the minimum value
     * it will become the minimum value.
     *
     * @param a The scalar value to multiply this matrix.
     * @param min Minimum value possible.
     * @return A new matrix scaled by the given value.
     */
    public Matrix minScale(double a, double min){
        double[][] array = new double[rows][columns];

        for(int i=0;i<rows;i++){
            for(int j=0;j<columns;j++){
                array[i][j] = a * values[i][j];
                if(array[i][j] < min) array[i][j] = min;
            }
        }

        return new Matrix(array);
    }

    /**
     * Calculate the inverse matrix of this matrix. Only a non singular square
     * matrix has an inverse matrix, if this method is attempted on a singular
     * or non square matrix an UnsupportedOperationException will be thrown with
     * the description of the error.
     *
     * @return The inverse matrix.
     */
    public Matrix inverse(){
        double determinant = this.determinant();
        if(determinant == 0) throw new UnsupportedOperationException("Cannot "+
                "calculate the inverse matrix of a singular matrix");

        Matrix adj = this.adjugate();
        double array[][] = new double[adj.rows][adj.columns];
        for(int i=0;i<adj.rows;i++){
            for(int j=0;j<adj.columns;j++){
                array[i][j] = adj.values[i][j] / determinant;
            }
        }

        return new Matrix(array);
    }

    /**
     * Create the transpose of this matrix.
     *
     * @return Transpose of this matrix.
     */
    public Matrix transpose(){
        double[][] array = new double[columns][rows];

        for(int i=0;i<columns;i++){
            for(int j=0;j<rows;j++){
                array[i][j] = values[j][i];
            }
        }

        return new Matrix(array);
    }

    private Matrix adjugate(){
        Matrix cofactors = this.cofactors();
        return cofactors.transpose();
    }

    private Matrix cofactors(){
        Matrix minors = this.minors();
        double[][] array = new double[minors.rows][minors.columns];

        for(int i=0;i<array.length;i++){
            for(int j=0;j<array[i].length;j++){
                array[i][j] = ((i%2!=0 ^ j%2!=0)? -1:1) * minors.values[i][j];
            }
        }

        return new Matrix(array);
    }

    private Matrix minors(){
        double[][] array = new double[rows][columns];

        for(int i=0;i<array.length;i++){
            for(int j=0;j<array[i].length;j++){
                array[i][j] = this.removeRowColumn(i,j).determinant();
            }
        }

        return new Matrix(array);
    }

    /**
     * Calculate the determinant of this matrix. This method throws an
     * unreported UnsupportedOperationException if the matrix isn't square.
     *
     * @return The determinant of this matrix.
     */
    public double determinant(){
        if(rows != columns) throw new UnsupportedOperationException("Cannot "+
                "calculate the determinant on a non square matrix");

        //if(rows==2) return values[0][0]*values[1][1]-values[1][0]*values[0][1];
        if(rows==1) return values[0][0];

        // Laplace Method
        double sum = 0;
        for(int i=0;i<columns;i++){
            double det = removeRowColumn(0,i).determinant();
            sum += ((i%2!=1)? 1 : -1) * values[0][i] * det;
        }

        return sum;
    }

    /**
     * Calculate the norm of this matrix.
     *
     * @return The value of the norm.
     */
    public double norm(){
        double sum = 0;
        for(int i=0;i<rows;i++) for(int j=0;j<columns;j++){
            sum += values[i][j] * values[i][j];
        }
        return Math.sqrt(sum);
    }

    /**
     * Change the value on a given position of the matrix.
     *
     * @param newValue New value to put on the given position.
     * @param row Row of the value to replace.
     * @param column Column of the value to replace.
     * @return New matrix with the new value replaced.
     */
    public Matrix changeValue(double newValue, int row, int column){
        if(row<0 || row>=rows)
            throw new IllegalArgumentException(String.format("Cannot access "+
                        "row %d from %d rows.",row,rows));
        if(column<0 || column>=columns)
            throw new IllegalArgumentException(String.format("Cannot access "+
                        "column %d from %d columns",column,columns));

        double[][] array = new double[rows][columns];

        for(int i=0;i<rows;i++) for(int j=0;j<columns;j++){
            if(i!=row || j!=column) array[i][j] = values[i][j];
            else array[i][j] = newValue;
        }

        return new Matrix(array);
    }

    /**
     * Remove the given row and column form this matrix. The result is a new
     * matrix with n-1 rows and m-1 columns with the values from the original
     * matrix without those on the given row and column.
     *
     * @param row The row to remove from the matrix.
     * @param column The column to remove from the matrix.
     * @return This matrix without the given row and column.
     */
    public Matrix removeRowColumn(int row, int column){
        Matrix temp = this.removeRow(row);
        return temp.removeColumn(column);
    }

    /**
     * Remove the given row form this matrix. The result is a new matrix with 
     * n-1 rows with the values from the original matrix without those on the
     * given row.
     *
     * @param row The row to remove from the matrix.
     * @return This matrix without the given row.
     */
    public Matrix removeRow(int row){
        if(row<0 || row>=rows)
            throw new IllegalArgumentException(String.format("Cannot remove "+
                        "row %d from %d rows.",row,rows));

        double[][] array = new double[rows-1][columns];
        int r = 0;
        for(int i=0;i<rows;i++) if(i!=row){
            array[r++] = Arrays.copyOf(values[i],columns);
        }

        return new Matrix(array);
    }

    /**
     * Remove the given column form this matrix. The result is a new matrix 
     * with m-1 columns with the values from the original matrix without those
     * on the given column.
     *
     * @param column The column to remove from the matrix.
     * @return This matrix without the given column.
     */
    public Matrix removeColumn(int column){
        if(column<0 || column>=columns)
            throw new IllegalArgumentException(String.format("Cannot remove "+
                        "column %d from %d columns",column,columns));

        double[][] array = new double[rows][columns-1];
        for(int i=0;i<rows;i++){
            int col = 0;
            for(int j=0;j<columns;j++) if(j!=column){
                array[i][col++] = values[i][j];
            }
        }

        return new Matrix(array);
    }

    /**
     * Gets the value of at the given row and column.
     *
     * @param row The row where the value is located.
     * @param column The column where the value is located.
     * @return The double that is store at the given row and column.
     */
    public double getValue(int row, int column){
        if(rows < 0 || row>rows || column<0 || column>columns)
            throw new IllegalArgumentException(String.format("Cannot retrive"+
                       " the value at %d,%d on a %dx%d matrix",row,column,rows,
                       columns));
        
        return values[row][column];
    }

    /**
     * Get the nth value of the matrix.
     *
     * @param n Index of the value to get from the matrix.
     * @return Nth value from the matrix.
     */
    public double getValue(int n){
        return getValue(n/columns,n%columns);
    }

    /**
     * Get the array of values from this matrix.
     *
     * @return Bidimensional array representing this matrix.
     */
    public double[][] toArray(){
        double[][] val = new double[rows][columns];
        for(int i=0;i<rows;i++) val[i] = Arrays.copyOf(values[i],columns);
        return val;
    }

    /**
     * Creates a new matrix (representing a row Vector) with one row and the
     * values stored on this matrix at the given row on the columns.
     *
     * @param row The row to get.
     * @return New matrix with the values at that row.
     */
    public Matrix getRow(int row){
        double[][] array = new double[1][columns];

        for(int i=0;i<columns;i++){
            array[0][i] = values[row][i];
        }

        return new Matrix(array);
    }

     /**
     * Creates a new matrix (representing a column Vector) with one column and
     * the values stored on this matrix at the given column on the rows. 
     *
     * @param column The column to get.
     * @return New matrix with the values at that column.
     */
    public Matrix getColumn(int column){
        double[][] array = new double[rows][1];

        for(int i=0;i<rows;i++){
            array[i][0] = values[i][column];
        }

        return new Matrix(array);
    }

    /**
     * Get how many rows this matrix has.
     *
     * @return Number of rows of this matrix.
     */
    public int rows(){ return rows; }

     /**
     * Get how many columns this matrix has.
     *
     * @return Number of columns of this matrix.
     */
    public int columns(){ return columns; }

    @Override
    public String toString(){
        String s = "";
        for(double[] row : values){
            s += "| ";
            for(double val : row){
                s += String.format("%7.2f ",val);
            }
            s += " |\n";
        }
        return s;
    }

    // Test method
    public static void main(String[] args){
        /*
        double[][] array = new double[2][2];
        for(int i=0;i<array.length;i++){
            for(int j=0;j<array[i].length;j++){
                array[i][j] = i*array[i].length + j ;
            }
        }
        */

        /*
        double[][] array = {{1, 2, 3},{3, 2, 1},{1, 2, 1}};

        Matrix m = new Matrix(array);
        System.out.printf("Matrix m:\n%s\n",m.toString());
        System.out.printf("Det(m) = %.2f\n",m.determinant());

        System.out.printf("Inverse:\n%s\n",m.inverse());

        System.out.printf("Transpose m:\n%s\n",m.transpose());
        System.out.printf("Det(transpose) = %.2f\n",m.transpose().determinant());
        */

        double[][] array1 = {{1,1,1,1},{8,4,2,1},{27,9,3,1},{216,36,6,1}};
        Matrix v1 = new Matrix(array1);

        double[][] array2 = {{1},{2},{4},{5}};
        Matrix v2 = new Matrix(array2);

        System.out.printf("v1:\n%s\nv2:\n%s\n",v1,v2);
        System.out.printf("inv(v1)*v2 =\n%s\n",v1.inverse().product(v2));

        /*
        Matrix[] unit = Matrix.unitVectors(v2);
        for(int i=0;i<unit.length;i++){
            System.out.printf("%s\n",unit[i]);
        }
        */
    }
}
