/**
 * Represent a point on x y plane.
 *
 * @author RAbrantes
 */

package math;

import java.util.Arrays;

public class Point{
    private final double[] coords;
    private final int dim;

    /**
     * Create a new point at the given coordinates.
     *
     * @param x Double with the x coordinates of the new point.
     * @param y Double with the y coordinates of the new point.
     */
    public Point(double x, double y){
        coords = new double[2];
        dim = 2;
        coords[0] = x; 
        coords[1] = y; 
    }

    /**
     * Create a new point at the given coordinates.
     *
     * @param x Double with the x coordinates of the new point.
     * @param y Double with the y coordinates of the new point.
     * @param z Double with the z coordinates of the new point.
     */
    public Point(double x, double y, double z){
        coords = new double[3];
        dim = 3;
        coords[0] = x; 
        coords[1] = y; 
        coords[2] = z; 
    }

    /**
     * Create a new point at the given coordinates.
     *
     * @param point Double array with the coordinates of the new point.
     */
    public Point(double[] point){
        if(point == null) throw new NullPointerException("Invalid point array"+
                ": Double array point can't be null.");

        dim = point.length;
        coords = Arrays.copyOf(point,dim);
    }

    /**
     * Get the x coordinate of this point.
     *
     * @return Double value of the x coordinate of this point.
     */
    public double x(){
        if(dim<1) throw new UnsupportedOperationException("This point has no"+
               " dimensions.");
        return coords[0];
    }

    /**
     * Get the y coordinate of this point.
     *
     * @return Double value of the y coordinate of this point.
     */
    public double y(){
        if(dim<2) throw new UnsupportedOperationException("This point has only"+
               " one dimension.");
        return coords[1];
    }

    /**
     * Get the z coordinate of this point.
     *
     * @return Double value of the z coordinate of this point.
     */
    public double z(){
        if(dim<2) throw new UnsupportedOperationException("This point has only"+
               " two dimensions.");
        return coords[2];
    }

    /**
     * Get the nth coordinate of this point.
     *
     * @param n Dimension number.
     * @return Nth coordinate of this point.
     */
    public double getCoord(int n){
        if(dim<n) throw new UnsupportedOperationException("This point doesn't"+
               " have that many dimensions.");
        return coords[n];
    }

    /**
     * Eucledean distance from this point to that. Dimensions between the two
     * points must agree.
     *
     * @param p End point.
     * @return Distance to the given point.
     */
    public double distanceTo(Point p){
        if(dim!=p.dim) throw new IllegalArgumentException("Dimensions must "+
                "agree to calculate the distance between two points.");

        double sum = 0.0;
        for(int i=0;i<dim;i++) sum += Math.pow(p.coords[i]-coords[i],2);
        return Math.sqrt(sum);
    }

    @Override
    public String toString(){
        String s = "(";
        for(int i=0;i<dim-1;i++) s += String.format("%.2f,",coords[i]);
        return s + String.format("%.2f)",coords[dim-1]);
    }
}
