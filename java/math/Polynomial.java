/**
 * Represents a one variable polynomial function.
 *
 * @author RAbrantes
 */

package math;

import java.util.Arrays;

public class Polynomial{
    private final double[] coeficients;

    /**
     * Create a new polinomial function given the coeficients on a double array.
     *
     * @param coeficients The coeficient values stored on a double array.
     * @return An polynomial with the given coeficients.
     */
    public Polynomial(double[] coeficients){
        if(coeficients == null) throw new NullPointerException("Coeficients " +
                "argument cannot be null.");

        this.coeficients = Arrays.copyOf(coeficients,coeficients.length);
    }

    /**
     * Create a new polinomial function given the coeficients on a matrix 
     * object.
     *
     * @param coeficients The coeficient values stored on a matrix object.
     * @return An polynomial with the given coeficients.
     */
    public Polynomial(Matrix coeficients){
        if(coeficients == null) throw new NullPointerException("Coeficients " +
                "argument cannot be null.");

        if(coeficients.rows() != 1 && coeficients.columns() != 1)
            throw new IllegalArgumentException("Invalid coeficients matrix: "+
                    "It must represent a Vector.");

        this.coeficients = new double[coeficients.rows() * 
            coeficients.columns()];
        for(int i=0;i<this.coeficients.length;i++){
            this.coeficients[i] = coeficients.getValue(i);
        }
    }

    /**
     * Create a new polynomial that tries to approach the given points.
     *
     * @param points Points that this polynome should approach.
     * @return New polinomial that approaches the given points.
     */
    public static Polynomial fromPoints(Point[] points){
        if(points == null) throw new NullPointerException("Argument passed is"+
                " null.");

        double[][] arrayA = new double[points.length][points.length];
        double[][] arrayB = new double[points.length][1];

        for(int i=0;i<points.length;i++){
            for(int j=0;j<points.length;j++){
                arrayA[i][j] = Math.pow(points[i].x(),points.length-j-1);
            }
            arrayB[i][0] = points[i].y();
        }

        Matrix a = new Matrix(arrayA);
        Matrix b = new Matrix(arrayB);

        return new Polynomial(a.inverse().product(b));
    }

    /**
     * Calculate the value of the polynomial function at the given ordinate.
     *
     * @param x Value of the x position where to calculate the value of this
     * polynomial.
     * @return Double value of the function evaluated at the given position.
     */
    public double value(double x){
        double sum = 0;
        for(int i=0;i<coeficients.length;i++){
            sum += coeficients[i] * Math.pow(x,coeficients.length-i-1);
        }
        return sum;
    }

    /**
     * Calculate the derivative of this polynomial.
     *
     * @return A new polynomial representing the derivative.
     */
    public Polynomial derivative(){
        if(coeficients.length==1) return new Polynomial(new double[1]);
        
        double[] coef = new double[coeficients.length-1];
        for(int i=0;i<coef.length;i++){
            coef[i] = coeficients[i] * (coeficients.length - i - 1);
        }
        return new Polynomial(coef);
    }

    /**
     * Get the degree of this polynomial.
     *
     * @return Degree of the polynomial.
     */
    public int degree(){
        return coeficients.length-1;
    }

    /**
     * Find roots of this polynome. Currently only implementd for 2nd and first
     * order polynomes.
     *
     * @return Array with real roots of the polynome, null if none are found.
     */
    public double[] roots(){
        if(degree() > 2) throw new UnsupportedOperationException("Root calcula"+
                "tion not implemented for cubic functions and above.");

        double[] root = null;
        if(degree()==2){
            double a,b,c, temp;
            a = coeficients[0];
            b = coeficients[1];
            c = coeficients[2];
            temp = b*b-4*a*c;
            if(temp < 0) return null;
            else if(temp == 0){
                root = new double[1];
                root[0] = -b/2/a;
            }else{
                temp = Math.sqrt(temp);
                root[0] = (-b-temp)/2/a;
                root[1] = (-b+temp)/2/a;
            }
        }else{
            root = new double[1];
            root[0] = -coeficients[1]/coeficients[0];
        }

        return root;
    }

    /**
     * Try to find minimums of this polynome.
     *
     * @return x value where minimums occur.
     */
    public double[] min(){
        double[] x = new double[1];

        Polynomial d1 = derivative();
        Polynomial d2 = d1.derivative();

        double[] roots = d1.roots();
        if(roots==null) throw new UnsupportedOperationException("Can't find a"+
                "minimum.");
        else if(roots.length==1){
            if(d2.value(roots[0])>0){
               x[0] = roots[0];
               return x;
            }
        }else{
            if(d2.value(roots[0])>0){
                x[0] = roots[0];
                return x;
            }
            else if(d2.value(roots[1])>0){
                x[0] = roots[1];
                return x;
            }
        }
        return null;
    }

    /**
     * Try to find maximums of this polynome.
     *
     * @return x value where maximums occurs.
     */
    public double[] max(){
        double[] x = new double[1];

        Polynomial d1 = derivative();
        Polynomial d2 = d1.derivative();

        double[] roots = d1.roots();
        if(roots==null) throw new UnsupportedOperationException("Can't find a"+
                "minimum.");
        else if(roots.length==1){
            if(d2.value(roots[0])<0){
               x[0] = roots[0];
               return x;
            }
        }else{
            if(d2.value(roots[0])<0){
                x[0] = roots[0];
                return x;
            }
            else if(d2.value(roots[1])<0){
                x[0] = roots[1];
                return x;
            }
        }
        return null;
    }

    @Override
    public String toString(){
        String s = "";  
        for(int i=0;i<coeficients.length;i++){
            s += String.format("%.2f",coeficients[i]);
            if(i!=coeficients.length - 1){
                if(i == coeficients.length - 2) s += " * x + ";
                else s += String.format(" * x^%d + ",degree()-i);
            }
        }
        return s;
    }

    public static void main(String[] args){
        double[] d1 = {1,4};
        double[] d2 = {3,2};
        double[] d3 = {5,3};
        double[] d4 = {7,8};

        Point p1 = new Point(d1);
        Point p2 = new Point(d2);
        Point p3 = new Point(d3);
        Point p4 = new Point(d4);

        Point[] points = {p1,p2,p3,p4};

        Polynomial poly = Polynomial.fromPoints(points);

        for(int i=0;i<points.length;i++) System.out.printf("%s ",points[i]);
        System.out.printf("\n%s",poly);
        System.out.printf(" (x=0) -> f(x) = %f\n",poly.value(0));
        System.out.printf("\n%s",poly.derivative());
        System.out.printf(" (x=0) -> f(x) = %f\n",poly.derivative().value(0));
        System.out.printf("\n%s",poly.derivative().derivative());
        System.out.printf(" (x=0) -> f(x) = %f\n",poly.derivative().derivative().value(0));
    }
}
