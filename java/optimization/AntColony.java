/**
 * Ant Colony optimization implementation.
 *
 * @author RAbrantes
 */

package optimization;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.util.Arrays;
import java.util.Comparator;

import java.util.concurrent.Phaser;
import java.util.concurrent.ThreadLocalRandom;

import math.Matrix;

public class AntColony implements Solver{
    private final Phaser phaser;
    private final Thread[] ants;

    private final Benchmark problem;
    private final Matrix distances;
    private final Matrix visibility;
    private final int n;

    private File logFile;

    private Matrix pheromone;
    private final Ant[] colony;

    private final double a, b, p, q; //Alpha Beta Rho & Q
    private final boolean elitism, ranking;
    private final int elitistAnts, rankAnts;

    private Matrix mMin;
    private double min;

    private final Object lock = new Object();

    /**
     * Create a new ant colony solver.
     *
     * @param problem Benchmark problem instance.
     * @param antNumber Number of ants to simulate.
     * @param alpha Pheromone influence parameter.
     * @param beta Greedy influence parameter.
     * @param rho Pheromone evaporation parameter.
     * @param Q New path pheromone deposit parameter.
     * @param sigma Number of elitist ants. If no elitism is to be considered
     * pass zero.
     * @param omega Number of ants to be considered at pheromone update. If no
     * ranking is to be considered pass zero.
     */
    public AntColony(Benchmark problem, int antNumber, double alpha,
            double beta, double rho, double Q, int sigma, int omega){

        this.problem = problem;
        a = alpha;
        b = beta;
        p = rho;
        q = Q;
        distances = problem.getDistances();

        elitism = sigma > 0;
        ranking = omega > 0;

        if(ranking && !elitism) throw new IllegalArgumentException("Ranking "+
            "cannot be applied without elitism!");

        elitistAnts = sigma;
        rankAnts = omega;

        logFile = null;

        n = distances.rows();
        double[][] array = new double[n][n];
        for(int i=0;i<n;i++) for(int j=0;j<n;j++) if(i!=j){
            array[i][j] = 1.0 / distances.getValue(i,j);
        }
        visibility = new Matrix(array);
        
        phaser = new Phaser();
        pheromone = Matrix.ones(n);
        colony = new Ant[antNumber];
        ants   = new Thread[antNumber];

        phaser.register();
        for(int i=0;i<antNumber;i++){
            colony[i] = new Ant(phaser);
            ants[i] = new Thread(colony[i]);
            ants[i].setDaemon(true);
        }
    }
    
    /**
     * Setup a log file for the optimization operation.
     *
     * @param file File where the log will be written.
     */
    public void setupLogFile(File file){
        logFile = file;
    }

    private Matrix calcPheromones(){
        double[][] array = new double[n][n];
        Matrix delta = Matrix.zeros(n);
        
        if(elitism) delta = delta.sum(mMin.scale(elitistAnts*q/min));

        if(!ranking){
            for(Ant ant : colony){
                delta = delta.sum(ant.pathMatrix.scale(q/ant.elapsedTime));
            }
        }else{
            int i = 1;
            for(int j=colony.length-1;i<rankAnts;j--,i++){
                delta = delta.sum(colony[j].pathMatrix.scale((rankAnts - i) *
                        1.0 * q / colony[j].elapsedTime));
            }
        }

        Matrix f = delta.sum(pheromone.scale(p));
        f = f.minScale(1,1e-08);
        return f;
    }

    @Override
    public Solution solve(){
        for(Thread ant : ants) ant.start();
        phaser.arriveAndAwaitAdvance();

        String log = "";
        //System.out.printf("%4s\t%6s\t%6s\t%6s\n","It.","Calls","Best","ItBest");

        min = Double.MAX_VALUE;
        mMin = null;
        int status = 0;
        do{
            status = phaser.arriveAndAwaitAdvance();

            double localMin = Double.MAX_VALUE;
            if(ranking){
                try{
                    Arrays.sort(colony, compare);
                }catch(IllegalArgumentException e){
                    //Do nothing, this error occurs when all ants share the same
                    //path, oddly common with a strong elitism pheromones.
                }
                if(min > colony[colony.length-1].elapsedTime){
                    min = colony[colony.length-1].elapsedTime;
                    mMin = colony[colony.length-1].pathMatrix;
                }
                localMin = colony[colony.length-1].elapsedTime;
            }else{
                for(Ant ant : colony){
                    if(min > ant.elapsedTime){
                        min = ant.elapsedTime;
                        mMin = ant.pathMatrix;
                    }
                    if(localMin > ant.elapsedTime) localMin = ant.elapsedTime;
                }
            }

            pheromone = calcPheromones();

            log += String.format("%d,%d,%f\n",status-1,problem.calls(),
                    min);
            //System.out.printf("%04d\t%6d\t%6.2f\t%6.2f\n",status-1,
                    //problem.calls(),min,localMin);
        }while(problem.calls() < 1e5 && phaser.arriveAndAwaitAdvance() >= 0);

        phaser.forceTermination();

        if(logFile != null){
            try(FileWriter fw = new FileWriter(logFile)){
                fw.write(log,0,log.length());
            }catch(IOException e){
                System.out.println("Could not write to logFile due to an IO "+
                        "problem ");
            }
        }

        return new Solution(mMin);
    }

    private static Comparator<Ant> compare = (Ant a1, Ant a2) -> {
        double diff = a1.elapsedTime - a2.elapsedTime;
        if(diff>0) return 1;
        else if(diff<0) return -1;
        else return 0;
    };

    private class Ant implements Runnable{
        private final Phaser phaser;

        private int[] path;
        private double elapsedTime;
        private Matrix pathMatrix;

        {
            path = new int[n+1];
        }

        private Ant(Phaser p){
            phaser = p;
            phaser.register();
        }

        private double[] probabilities(int[] unvisited, int current){
            double[] probability = new double[unvisited.length];
            for(int to : unvisited) if(to>0) {
                double num = Math.pow(pheromone.getValue(current,to),a) *
                        Math.pow(visibility.getValue(current,to),b);

                double den = 0.0;
                for(int h : unvisited) if(h>0) 
                    den += Math.pow(pheromone.getValue(current,h),a) *
                            Math.pow(visibility.getValue(current,h),b);

                double r = num / den;
                probability[to-1] = num / den;
            }
            return probability;
        }

        private int choose(double[] probability){
            double c = ThreadLocalRandom.current().nextDouble();

            double[] prob = Arrays.copyOf(probability,probability.length);
            if(c < prob[0]) return 0;
            for(int i=1;i<prob.length;i++){
                prob[i] += prob[i-1];
                if(c<prob[i]) return i;
            }
            return -1; // Error
        }

        @Override
        public void run(){
            int status = 0;
            while(status>=0){
                status = phaser.arriveAndAwaitAdvance();

                double[] probability;
                int[] unvisited = new int[n-1];
                for(int i=0;i<n-1;i++) unvisited[i] = i+1;

                path[0] = 0;
                for(int t=1;t<n-1;t++){
                    int current = path[t-1];

                    int chosen = -1;
                    probability = probabilities(unvisited,current);

                    // probability cannot be used after this line, 
                    // values change with call to choose, if needed 
                    // implement safe copy at choose method (not 
                    // implemented for increased performance)
                    chosen = choose(probability);
                    path[t] = chosen + 1;
                    try{
                    unvisited[chosen] = -1;
                    }catch(IndexOutOfBoundsException e){
                        /*
                        synchronized(lock){
                        System.out.println();
                        for(int i=0;i<probability.length;i++) System.out.printf("%.2e ",probability[i]);
                        System.out.println();
                        }
                        for(int i=0;i<pheromone.rows();i++){
                            for(int j=0;j<pheromone.columns();j++){
                                int bool = (pheromone.getValue(i,j) > 0)?1:0;
                                System.out.printf("%d",bool);
                            }
                            System.out.println();
                        }
                        */
                        throw new NullPointerException();
                    }
                }

                int end = 0;
                while(unvisited[end]<0) end++;
                path[n-1] = end + 1;
                path[n] = 0;

                Solution pathSolution = Benchmark.pathToSolution(path);
                elapsedTime = problem.evaluate(pathSolution);
                pathMatrix = pathSolution.getMatrix();

                status = phaser.arriveAndAwaitAdvance();
            }
        }
    }

    public static void main(String[] args){
        int n = 10;

        double alpha = 0.0001;
        double beta  = 14.0;
        int sigma = 55;
        int omega = 40;

        double mean, min, max, sum;
        int[] pMin, pMax;
        pMin = null;

        long startTime, endTime;

        Benchmark b;
        AntColony a;

        long remaining = n * 2;
        long processed = 0;
        long status, last;
        last = -1;
        status = 0;
        System.out.println();
        for(int i=0;i<2;i++){
            String name = null;
            int s, o;
            s = o = 0;
            switch(i){
                case 0:
                    name = "AC";
                    break;
                case 1:
                    name = "ACelite";
                    s = sigma;
                    break;
                case 2:
                    name = "ACrank";
                    s = sigma;
                    o = omega;
                    break;
            }

            startTime = System.nanoTime();
            min = Double.MAX_VALUE;
            for(int k=0;k<n;k++){
                if(status != last){
                    last = status;
                    System.out.printf("\rProcessing... %3d%% Done",status);
                }

                b = new Benchmark(args[0],300,519);
                a = new AntColony(b,100,alpha,beta,0.5,100,s,o);
                a.setupLogFile(new File(String.format("%s_%03d.log",name,k)));

                Solution sol = a.solve();
                double val = b.evaluate(sol);
                if(val < min){
                    min = val;
                    pMin = Benchmark.solutionToPath(sol);
                }

                processed++;
                status = Math.round(processed * 100.0 / remaining); 
            }
            endTime = System.nanoTime();

            System.out.printf("\nRuntime = %.2f s\n",(startTime - endTime) *
                    1.0e-9);

            System.out.printf("Min path length: %.2f",min);
            System.out.print("Min Path\n[ ");
            for(int node : pMin) System.out.printf("%d ",node);
            System.out.println("]");
        }
        System.out.println("\rProcessing... Done!       \n");
    }
}
