/**
 * 2019 Benchmark problem implementation.
 *
 * @author RAbrantes
 */

package optimization;

import java.io.File;
import java.io.FileNotFoundException;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

import java.util.concurrent.locks.ReentrantLock;

import math.Matrix;
import math.Point;

public class Benchmark extends Problem{
    private final ReentrantLock rl;
    private long calls;

    private final Point[] points;

    private final Matrix distances;

    {
        rl = new ReentrantLock();
        calls = 0;
    }

    /**
     * Create a benchmark problem. Point data collected from the given file.
     * Tool speeds must be given for upward and downward motion.
     *
     * @param fileName String with the file name and, if necessary, path to file
     * directory.
     * @param upwardSpeed Speed in mm/s for the tool when moving upward in the
     * z axis.
     * @param downwardSpeed Speed in mm/s for the tool when movin downward in
     * the z axis.
     */
    public Benchmark(String fileName, double upwardSpeed, double downwardSpeed){
        points = readFile(fileName, -1);
        distances = calcDistances(points,upwardSpeed,downwardSpeed);
    }

    /**
     * Get the matrix of the times between points of this benchmark.
     *
     * @return Square matrix holding information about the time required to go 
     * from the point with the same index as the row to the point with the 
     * column index.
     */
    public Matrix getDistances(){
        return distances;
    }

    private Point[] readFile(String fileName, int lines){
        LinkedList<Point> pointList = new LinkedList<Point>();
        try(Scanner sc = new Scanner(new File(fileName))){
            sc.useDelimiter("\\s");
            sc.nextLine();
            
            int i = 0;
            while(sc.hasNextDouble() && (i++<lines || lines<0)){
                double x = sc.nextDouble();
                double y = sc.nextDouble();
                double z = sc.nextDouble();
                pointList.add(new Point(x,y,z));
                sc.nextLine();
            }
        }catch(FileNotFoundException e){
            throw new IllegalArgumentException("Invalid file name: "+fileName);
        }
        return pointList.toArray(new Point[0]);
    }

    private Matrix calcDistances(Point[] points, double upward,
            double downward){

        int size = points.length;
        double[][] array = new double[size][size];
        for(int i=0;i<size;i++) for(int j=0;j<size;j++) if(i!=j){
            double speed = (points[i].z() < points[j].z())? upward:downward;
            array[i][j] = points[i].distanceTo(points[j]) / speed;
        }
        return new Matrix(array);
    }

    private static boolean assertSolution(Solution s){
        Matrix sol = s.getMatrix();

        if(sol.rows() != sol.columns()) return false;

        for(int i=0;i<sol.rows();i++){
            int nLine = 0;
            for(int j=0;j<sol.columns();j++){
                if(sol.getValue(i,j) > 0.0) nLine++;
            }
            if(nLine != 1) return false;
        }

        for(int j=0;j<sol.columns();j++){
            int nColumn = 0;
            for(int i=0;i<sol.rows();i++){
                if(sol.getValue(i,j) > 0.0) nColumn++;
            }
            if(nColumn != 1) return false;
        }

        return true;
    }

    @Override
    public long calls(){
        long calls;

        rl.lock(); 
        try{
            calls = this.calls;
        }finally{
            rl.unlock();
        }

        return calls;
    }

    @Override
    public double evaluate(Solution s){
        if(!assertSolution(s)) throw new IllegalArgumentException("Invalid "+
                "solution to the problem.");

        Matrix path = s.getMatrix();
        path = path.matrixDotProduct(distances);

        double time = 0.0;
        for(int i=0;i<path.rows()*path.columns();i++){
            time += path.getValue(i);
        }

        rl.lock();
        try{
            calls++;
        }finally{
            rl.unlock();
        }

        return time;
    }

    /**
     * Convert a solution of this problem in to an array of integers 
     * representing the indexes of points.
     *
     * @param s Solution to the benchmark to convert.
     * @return Array of integers with the indexes of the visited points in order
     * of visit.
     */
    public static int[] solutionToPath(Solution s){
        if(!assertSolution(s)) throw new IllegalArgumentException("Invalid "+
                "solution to the problem.");

        Matrix p = s.getMatrix();
        int n = p.rows();
        int[] path = new int[n+1];
        path[0] = 0;

        int heading = 0;
        int pos = 1;
        do{
            boolean found = false;
            for(int i=0;i<n && !found;i++) if(p.getValue(heading,i)>0.0){
                heading = i;
                found = true;
            }
            path[pos++] = heading;
        }while(heading != 0);

        return path;
    }

    /**
     * Convert an array of integer representing a path to a solution to this
     * problem.
     *
     * @param path Integer array containing the indexes of points.
     * @return A solution representing the path.
     */
    public static Solution pathToSolution(int[] path){
        int[] copy = Arrays.copyOf(path,path.length);
        Arrays.sort(copy);

        int last = -1;
        for(int i=1;i<copy.length;i++){
            if(copy[i]!=last+1 || copy[i] < 0)
                throw new IllegalArgumentException("Invalid path.");
            last = copy[i];
        }
        
        int n = path.length - 1;
        double[][] array = new double[n][n];

        int pos, heading, from;
        pos = heading = from = 0;
        do{
            from = heading;
            heading = path[++pos];
            array[from][heading] = 1.0;
        }while(heading!=0);

        return new Solution(new Matrix(array));
    }

    // Test method
    public static void main(String[] args){
        double[][] array = {{0,1,0},{0,0,1},{1,0,0}};
        Solution s = new Solution(new Matrix(array));

        int[] path = Benchmark.solutionToPath(s);
        for(int i = 0;i<path.length;i++) System.out.printf("%d ",path[i]);
        System.out.println();

        Solution n = Benchmark.pathToSolution(path);
        System.out.printf("\n%s\n",n.getMatrix());

        /*
        Benchmark b = new Benchmark("chapa.dat",1,1); //519,300
        double[][] d = {{0,0,1,0,0,0,0,0,0},{1,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,1,0,0},{0,0,0,0,1,0,0,0,0},{0,0,0,0,0,0,0,0,1},{0,1,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,1,0},{0,0,0,1,0,0,0,0,0},{0,0,0,0,0,1,0,0,0}};
        s = new Solution(new Matrix(d));
        System.out.printf("Tempo: %.2f",b.evaluate(s));
        */
    }

}
