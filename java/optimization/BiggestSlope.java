/**
 * Biggest slope algortithm implementation. Inefficiency in object form.
 *
 * @author RAbrantes
 */

package optimization;

import math.Matrix;

public class BiggestSlope implements Solver{
    private final DiferenciableProblem problem;
    private Solution s;

    /**
     * Create a new Biggest Slope solver for the given problem.
     *
     * @param dp Differenciable problem to be solved with this algorithm.
     * @param initialSolution Solution for the first iteration.
     */
    public BiggestSlope(DiferenciableProblem dp, Solution initialSolution){
        problem = dp;
        s = initialSolution;
    }

    private Solution nextX(Matrix dir, double value){
        Matrix pos = s.getMatrix();
        Matrix temp;
        Solution sTemp;

        double newValue, min, max, x;
        newValue = value;
        max= 1e-6;
        do{
            value = newValue;
            max *= 10;
            sTemp = new Solution(pos.sum(dir.scale(max)));
            newValue = problem.evaluate(sTemp);
        }while(newValue<value);

        min = max / 10;
        double maxValue = newValue;

        sTemp = new Solution(pos.sum(dir.scale(min)));
        double minValue = problem.evaluate(sTemp);

        while(max-min<1e-3){
            x = (min+max)/2;

            sTemp = new Solution(pos.sum(dir.scale(x)));
            newValue = problem.evaluate(sTemp);

        }

        return new Solution(pos); //REPLACE ME
    }

    @Override
    public Solution solve(){
        int i=0;
        double fLast, f;
        Matrix grad, d;
        Solution temp;

        do{
            f = problem.evaluate(s);
            grad = problem.gradient(s);
            d = grad.scale(-1/grad.norm());

            s = nextX(d,f);
        }while(true);
    }
}
