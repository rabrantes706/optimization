/**
 * Box dimensioning problem.
 *
 * @author RAbrantes
 */

package optimization;

import java.io.File;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import box.Box;
import box.Item;

import math.Polynomial;
import math.Point;
import math.Matrix;

public class BoxProblem extends DiferenciableProblem{
    private final double MAX_X_ERROR_1 = 1e-4;
    private final double MAX_X_ERROR_23 = 1e-5;

    private final Item[] items;
    private long calls;

    private final boolean restrictions;
    private final double cg;
    private double rg;

    {
        calls = 0;
    }

    /**
     * Create a box problem with the items stored in the given file.
     *
     * @param fileName String with the name of the file where items are stored.
     */
    public BoxProblem(String fileName){
        items = Item.readFromFile(new File(fileName)).toArray(new Item[1]);
        restrictions = false;
        cg = 0;
    }

    /**
     * Create a box problem with the items stored int the given file with
     * restrictions. Initial penalty constant is given, and penalty progression
     * must be greater than unit.
     *
     * @param fileName String with the name of the file where items are stored.
     * @param rgi Initial penalty constant, greater than zero.
     * @param cg Penalty constant progression factor, greater than one.
     */
    public BoxProblem(String fileName, double rgi, double cg){
        if(rgi<=0) throw new IllegalArgumentException("Invalid initial penalty"
                +" constant.");
        if(cg<=1) throw new IllegalArgumentException("Invalid penalty progress"
                +"ion factor.");

        items = Item.readFromFile(new File(fileName)).toArray(new Item[1]);
        restrictions = true;
        this.cg = cg;
        rg = rgi;
    }

    /**
     * Get a string with information about the boxes created with the given
     * solution.
     *
     * @param s Solution with optimization variables.
     * @return String with each box information.
     */
    public String boxInfo(Solution s){
        Box[] boxes = Box.createBoxes(items,solutionToLimits(s));
        String t = "";

        double cost = 0;
        for(int i=0;i<boxes.length;i++){
            t += String.format(" Box %d:\n",i+1);
            t += boxes[i].toString();
            t += "\n";

            cost += boxes[i].getCost();
        }
        t += String.format("Cost Sum: %.2f",cost);

        return t;
    }

    /**
     * Update the penalty constant.
     */
    public void updatePenalty(){
        rg *= cg;
    }

    /**
     * Check if the given solution is valid.
     */
    public boolean isValid(Solution s){
        Matrix m = s.getMatrix();
        for(int i=1;i<m.columns();i++){
            if(m.getValue(0,i-1) > m.getValue(0,i)) return false;
            for(int j=1;j<m.rows();j++){
                if(m.getValue(j-1,i) < m.getValue(j,i)) return false;
            }
        }
        return true;
    }

    private double[][] solutionToLimits(Solution s){
        Matrix m = s.getMatrix();
        Double[][] limits = new Double[m.columns()][];

        for(int i=0;i<limits.length;i++){
            ArrayList<Double> temp = new ArrayList<Double>(3);
            for(int j=0;j<m.rows();j++){
                double val = m.getValue(j,i);
                temp.add((Double)val);
            }

            limits[i] = new Double[temp.size()];
            for(int j=0;j<limits[i].length;j++) limits[i][j] = temp.get(j);
        }

        double[][] array = new double[limits.length][];
        for(int i=0;i<limits.length;i++){
            Arrays.<Double>sort(limits[i],Collections.reverseOrder());
            array[i] = new double[limits[i].length];
            for(int j=0;j<limits[i].length;j++)
                array[i][j] = (double)limits[i][j];
        }

        /*
        for(int i=0;i<array.length;i++){
            for(int j=0;j<array[i].length;j++){
                System.out.printf("%f ",array[i][j]);
            }
            System.out.println();
        }
        */
        return array;
    }

    private double solutionToCost(Solution s){
        double[][] limits = solutionToLimits(s);

        Box[] boxes = Box.createBoxes(items,limits);
        double cost = 0;
        for(Box box : boxes) cost += box.getCost();

        return cost;
    }

    private double derivative(Matrix s, int i, int j){
        Point center = new Point(s.getValue(i,j),evaluate(new Solution(s)));
        Point[] next = new Point[2];

        try{
            next[0] = left(s,i,j);
        }catch(IllegalArgumentException e){
            s = s.changeValue(next[0].x(),i,j);
            next[0] = right(s,i,j);
        }

        try{
            next[1] = right(s,i,j);
        }catch(IllegalArgumentException e){
            s = s.changeValue(next[0].x(),i,j);
            next[1] = left(s,i,j);
        }

        Point[] points = {next[0],center,next[1]};
        Polynomial poly = Polynomial.fromPoints(points);

        return poly.derivative().value(center.x());
    }

    private Point left(Matrix s, int i, int j){
        double x,min, max, value, newValue;
        Solution sTemp;

        value = evaluate(new Solution(s));

        x = max = s.getValue(i,j);
        do{
            x -= max * .01;
            s = s.changeValue(x,i,j);
            sTemp = new Solution(s);
        }while(evaluate(sTemp) == value);

        min = x;
        double maxError = (i==0) ? MAX_X_ERROR_1 : MAX_X_ERROR_23;
        while(max-min<maxError){
            x = (max+min)/2;

            sTemp = new Solution(s.changeValue(x,i,j));
            newValue = evaluate(sTemp);

            if(newValue == value) max = x;
            else min = x;
        }

        return new Point(min,evaluate(new Solution(s.changeValue(min,i,j))));
    }

    private Point right(Matrix s, int i, int j){
        double x,min, max, value, newValue;
        Solution sTemp;

        value = evaluate(new Solution(s));

        x = min = s.getValue(i,j);
        do{
            x += min * .01;
            s = s.changeValue(x,i,j);
            sTemp = new Solution(s);
        }while(evaluate(sTemp) == value);

        max = x;
        double maxError = (i==0) ? MAX_X_ERROR_1 : MAX_X_ERROR_23;
        while(max-min<maxError){
            x = (max+min)/2;

            sTemp = new Solution(s.changeValue(x,i,j));
            newValue = evaluate(sTemp);

            if(newValue == value) min = x;
            else max = x;
        }

        return new Point(max,evaluate(new Solution(s.changeValue(max,i,j))));
    }

    @Override
    public long calls(){ return calls; }

    @Override
    public double evaluate(Solution s){
        if(s==null) throw new NullPointerException("Invalid Solution: Null");

        Matrix m = s.getMatrix();

        if(m.rows()<1||m.rows()>3) throw new IllegalArgumentException("The gi"+
                "ven solution isn't a valid solution to this problem, the "+
                "number of rows is invalid.");
        
        calls++;
        double cost = solutionToCost(s);

        if(restrictions){
            double last = m.getValue(0,0);
            for(int i=1;i<m.columns();i++){
                double now = m.getValue(0,i);
                double diff = last - now;
                if(diff>0) cost += rg * diff*diff;
                last = now;
            }

            for(int i=0;i<m.columns();i++){
                last = m.getValue(0,i);
                for(int j=0;j<m.rows();j++){
                    double now = m.getValue(j,i);
                    double diff = last - now;
                    if(diff<0) cost += rg * diff*diff;
                    last = now;
                }
            }
        }

        return cost;
    }

    @Override
    public Matrix gradient(Solution s){
        if(s==null) throw new NullPointerException("Invalid Solution: Null");

        Matrix m = s.getMatrix();

        if(m.rows()<1||m.rows()>3) throw new IllegalArgumentException("The gi"+
                "ven solution isn't a valid solution to this problem, the "+
                "number of rows is invalid.");

        double[][] gradient = new double[m.rows()][m.columns()];
        for(int i=0;i<m.rows();i++) for(int j=0;j<m.columns();j++){
            gradient[i][j] = derivative(m,i,j);
        }

        return new Matrix(gradient);
    }

    // Test Method
    public static void main(String[] args){
        DiferenciableProblem dp = new BoxProblem(args[0]);

        double[][] array = new double[1][2];
        for(int i=0;i<1000;i++){
            array[0][0] = 1.0*i*2+1;

            Matrix m = new Matrix(array);
            double cost = dp.evaluate(new Solution(m));

            System.out.printf("%e,%e\n",array[0][0],cost);
        }
    }
}
