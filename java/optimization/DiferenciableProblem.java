/**
 * Sub-group of problems that can be diferenciated.
 *
 * @author RAbrantes
 */

package optimization;

import math.Matrix;

public abstract class DiferenciableProblem extends Problem{

    /**
     * Calculate the gradient of the loss function at the position of the given
     * solution.
     *
     * @param s Position where to calculate the gradient.
     * @return Matrix with the gradient value.
     */
    public abstract Matrix gradient(Solution s);
}
