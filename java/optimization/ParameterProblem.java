/**
 * Ant colony parameters optimization problem.
 *
 * @author RAbrantes
 */

package optimization;

import math.Matrix;

public class ParameterProblem extends Problem{
    private final Benchmark bench;
    private long calls;

    private final int antNumber;

    {
        calls = 0;
    }

    public ParameterProblem(Benchmark problem, int number){
        bench = problem;
        antNumber = number;
    }

    @Override
    public long calls(){
        return calls;
    }

    @Override
    public double evaluate(Solution s){
        double alpha, beta;
        int elite, rank;

        alpha = s.getMatrix().getValue(0);
        beta  = s.getMatrix().getValue(1);
        elite = (int)Math.round(s.getMatrix().getValue(2));
        rank  = (int)Math.round(s.getMatrix().getValue(3));

        AntColony ac = new AntColony(bench,antNumber,alpha,beta,.5,100,elite,rank);
        Solution acSolution = ac.solve();
        calls++;
        return bench.evaluate(acSolution);
    }

    public static void main(String[] args){
        Benchmark b = new Benchmark(args[0],300,519);
        ParameterProblem pp = new ParameterProblem(b,100);

        double[][] array = new double[1][4];
        array[0][0] = -0.1930; array[0][1] = 18.8; array[0][2] = 30; array[0][3] = 30;
        Matrix m = new Matrix(array);

        Solver solver = new Simplex(pp,new Solution(m),1e-4);
        Solution bestParameters = solver.solve();

        System.out.printf("\n%s\n",bestParameters.getMatrix());
    }
}
