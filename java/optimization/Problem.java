/**
 * Superclass of all problems in life.
 *
 * @author RAbrantes
 */

package optimization;

public abstract class Problem{

    /**
     * Get the number of times this problem has calculated the loss function.
     *
     * @return Long integer with the number of times the loss function was
     * calculated.
     */
    public abstract long calls();

    /**
     * Evaluate the loss function of this problem for the given solution.
     *
     * @param s Solution to evaluate.
     * @return Double of the value of the loss function for the solutuin s.
     */
    public abstract double evaluate(Solution s);
}
