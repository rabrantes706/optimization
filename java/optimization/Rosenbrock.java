/**
 * Two variable Rosenbrock function problem implementation.
 *
 * @author RAbrantes
 */

package optimization;

import math.Matrix;

public class Rosenbrock extends DiferenciableProblem{
    private long calls;

    {
        calls = 0;
    }

    @Override
    public long calls(){
        return calls;
    }

    @Override
    public double evaluate(Solution s){
        Matrix m = s.getMatrix();
        double x1 = m.getValue(0);
        double x2 = m.getValue(1);

        calls++;
        return 100 * Math.pow(x2-x1*x1,2) + Math.pow(1-x1,2);
    }

    @Override
    public Matrix gradient(Solution s){
        Matrix m = s.getMatrix();
        double x1 = m.getValue(0);
        double x2 = m.getValue(1);

        double[][] array = new double[2][1];
        array[0][0] = 400 * x1 * (x1*x1 - x2) - 2 * (1 - x1);
        array[0][1] = 200 * (x2 - x1*x1);
        calls++;
        return new Matrix(array);
    }

    public static void main(String[] args){
        double x1 = Double.parseDouble(args[0]);
        double x2 = Double.parseDouble(args[1]);

        double[][] d = {{x1},{x2}};
        Solution x = new Solution(new Matrix(d));

        Rosenbrock r = new Rosenbrock();
        System.out.printf("%8.4f\n",r.evaluate(x));
    }
}
