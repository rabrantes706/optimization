/**
 * Implementation of the Nelder and Mead simplex method.
 *
 * @author RAbrantes
 */

package optimization;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import math.Matrix;

public class Simplex implements Solver{
    private final Problem problem;

    private final Matrix[] simplex;
    private final double[] values;

    private final double a, r, e, c, s, error;
    private final int n;

    private File logFile;
    private String log;

    /**
     * Create a new nelder and mead solver for the given problem.
     *
     * @param p Problem to be solved with this solver.
     * @param initil Initial solution to be considered.
     * @param covThreshold Threshold for the size of the simplex to stop the
     * solver.
     */
    public Simplex(Problem p, Solution initial, double convThreshold){
        problem = p;

        a = 1000;
        r = 1.0;
        e = 2.0;
        c = .5;
        s = .5;
        error = convThreshold;

        Matrix m = initial.getMatrix();
        n = m.rows() * m.columns();

        simplex = new Matrix[n+1];
        values  = new double[n+1];
        createSimplex(m);
        evaluateSimplex();

        log = null;
    }

    /**
     * Setup a log file for the optimization operation.
     *
     * @param file File where the log will be written.
     */
    public void setupLogFile(File file){
        logFile = file;
        log = "";
    }

    private void createSimplex(Matrix initial){
        double p = a/n/Math.sqrt(2)*(Math.sqrt(n+1)+n-1);
        double q = a/n/Math.sqrt(2)*(Math.sqrt(n+1)-1);

        Matrix[] unit = Matrix.unitVectors(initial);
        simplex[0] = initial;
        for(int i=0;i<n;i++){
            Matrix point = initial.sum(unit[i].scale(p));

            Matrix sum = Matrix.zeros(initial.rows(),initial.columns());
            for(int j=0;j<n;j++) if(i!=j){
                sum = sum.sum(unit[j].scale(q));
            }

            simplex[i+1] = point.sum(sum);
        }

        if(log!=null){
            double eval = problem.evaluate(new Solution(initial));
            log += String.format("%d,%d,%e\n",0,problem.calls(),eval);
        }
    }

    private void evaluateSimplex(){
        for(int i=0;i<=n;i++)
            values[i] = problem.evaluate(new Solution(simplex[i]));
    }

    private Matrix centroid(int a){
        Matrix sum = Matrix.zeros(simplex[0].rows(),simplex[0].columns());
        for(int i=0;i<=n;i++) if(i!=a){
            sum = sum.sum(simplex[i]);
        }
        return sum.scale(1.0/n);
    }

    private Matrix centroid(){
        Matrix sum = Matrix.zeros(simplex[0].rows(),simplex[0].columns());
        for(int i=0;i<=n;i++) sum = sum.sum(simplex[i]);
        return sum.scale(1.0/(n+1));
    }

    private Matrix reflection(int a){
        return centroid(a).scale(1.0+r).sum(simplex[a].scale(-r));
    }

    private Matrix expansion(Matrix reflection, int a){
        return reflection.scale(e).sum(centroid(a).scale(1.0-e));
    }

    private Matrix contraction(int a){
        return simplex[a].scale(c).sum(centroid(a).scale(1.0-c));
    }

    private void reduction(int b){
        for(int i=0;i<=n;i++) if(i!=b){
            simplex[i] = simplex[i].scale(s).sum(simplex[b].scale(1.0-s));
        }
    }

    private boolean convergence(){
        double sum = 0;
        double centVal = problem.evaluate(new Solution(centroid()));
        for(int i=0;i<=n;i++) sum += Math.pow(values[i]-centVal,2);
        
        double Q = Math.sqrt(1.0/(1+n)*sum);
        //System.out.printf("\nQ=%8.4f\tcentVal=%8.4f\tsum=%8.4f\n",Q,centVal,sum);
        return Q <= error;
    }

    @Override
    public Solution solve(){
        int t = 1;
        int bestIndex;
        do{
            int a,b;
            double min = Double.MAX_VALUE;
            double max = Double.MIN_VALUE;

            a = b = -1;
            for(int i=0;i<=n;i++){
                double d = values[i];

                if(d<min){
                    min = d;
                    b = i;
                }
                if(d>max){
                    max = d;
                    a = i;
                }
            }

            log += String.format("%d,%d,%e\n",t,problem.calls(),min);

            Matrix ref = reflection(a);
            double valRef = problem.evaluate(new Solution(ref));

            String operation = null;
            if(valRef<min){
                Matrix exp = expansion(ref,a);
                double valExp = problem.evaluate(new Solution(exp));

                if(valExp<valRef){
                    simplex[a] = exp;
                    values[a] = valExp;
                    operation = "Expasion";
                }else{
                    simplex[a] = ref;
                    values[a] = valRef;
                    operation = "Reflection";
                }
            }else if(valRef<max){
                simplex[a] = ref;
                values[a] = valRef;
                operation = "Reflection";
            }else{
                Matrix con = contraction(a);
                double valCon = problem.evaluate(new Solution(con));

                if(valCon<max){
                    simplex[a] = con;
                    values[a] = valCon;
                    operation = "Contraction";
                }else{
                    reduction(b);
                    evaluateSimplex();
                    operation = "Reduction";
                }
            }

            /*
            for(Matrix point : simplex){
                for(int i=0;i<point.rows()*point.columns();i++){
                    System.out.printf("%f,",point.getValue(i));
                }
            }
            System.out.println();
            */

            /*
            System.out.printf("%3d\t%4d\t%.3e\t%s\n",t,
                    problem.calls(),values[b],operation);
            */

            t++;
            bestIndex = b;
        }while(!convergence() && t<1000);

        if(logFile != null){
            try(FileWriter fw = new FileWriter(logFile)){
                fw.write(log,0,log.length());
            }catch(IOException e){
                System.out.println("Could not write to logFile due to an IO "+
                        "problem ");
            }
        }

        return new Solution(simplex[bestIndex]);
    }

    public static void main(String[] args){
        double[][] init = { {250, 500, 750, 1000, 1250, 1500, 1750},
                      {250, 500, 750, 1000, 1250, 1500, 1750},
                      {250, 500, 750, 1000, 1250, 1500, 1750}};
        //double[][] init = {{1000, 1500}};
        Matrix m = new Matrix(init);
        //Matrix m = Matrix.ones(3,7).scale(1000.0);

        Solution initial = new Solution(m);
        BoxProblem bp = new BoxProblem("items.txt",1,2);

        Solution best = null;
        int t = 0;
        int rg = 1;
        double last ;
        double result = 0;
        do{
            last = result;
            Simplex s = new Simplex(bp, initial, 1e-9);
            //s.setupLogFile(new File(String.format("%d_%02d.log",1,t)));
            best = s.solve();
            result = bp.evaluate(best);

            //System.out.printf("\nIteration %d : rg = %d\n",t,rg);
            initial = best;
            bp.updatePenalty();
            rg *= 2;
            t++;

            //System.out.printf("\n%s\n",best.getMatrix());
        }while(!bp.isValid(best) || result != last && t<=10);


        System.out.println(bp.boxInfo(best));
    }
}
