/**
 * This class represents one solution of a problem. This can represent any valid
 * solution to a given problem, does not mean that is the best one; finding the
 * best solution to a problem is done by a solver.
 *
 * @author RAbrantes
 */

package optimization;

import math.Matrix;

public class Solution{
    private final Matrix solution;

    public Solution(Matrix solution){
        this.solution = solution;
    }

    /**
     * Retrieve the solution matrix.
     *
     * @return This solution matrix.
     */
    public Matrix getMatrix(){ return solution; }
}
