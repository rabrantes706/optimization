/**
 * Interface of all classes that solve problems.
 *
 * @author RAbrantes
 */

package optimization;

public interface Solver{

    /**
     * Solve the problem.
     *
     * @return The best solution this solver can find.
     */
    public Solution solve();
}
